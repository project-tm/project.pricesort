# Модуль каталога #

необходим модуль https://bitbucket.org/project-tm/project.core

* цены из разных валют, сохраняются в одной валюте в отдельном поле
* для торговых предложений, выясняется минимальная цена с учетом скидки


```
#!php

    /*
      "ELEMENT_SORT_FIELD" => "PROPERTY_SYSTEM_PRICE",
      "ELEMENT_SORT_ORDER" => "DESC",
     */

    /*
      Скрыть отсутствующие товары:

      "HIDE_NOT_AVAILABLE" => "N",
      Отсортировать по популярности, но показать сначала товары в наличии, затем отсутствующие:

      "ELEMENT_SORT_FIELD" => "CATALOG_AVAILABLE",
      "ELEMENT_SORT_ORDER" => "DESC",
      "ELEMENT_SORT_FIELD2" => "SHOWS",
      "ELEMENT_SORT_ORDER2" => "DESC",
      Отсортировать по наличию:

      "ELEMENT_SORT_FIELD" => "CATALOG_AVAILABLE",
      "ELEMENT_SORT_ORDER" => "DESC",
     */
```