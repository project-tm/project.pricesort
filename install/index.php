<?php

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Loader;
use Project\Tools\Modules;

Loader::includeModule('jerff.core');

Loc::loadMessages(__FILE__);

class project_pricesort extends CModule
{

    public $MODULE_ID = 'project.pricesort';

    use Modules\Install;

    function __construct()
    {
        $this->setParam(__DIR__, 'PROJECT_PRICESORT');
        $this->MODULE_NAME = Loc::getMessage('PROJECT_PRICESORT_NAME');
        $this->MODULE_DESCRIPTION = Loc::getMessage('PROJECT_PRICESORT_DESCRIPTION');
        $this->PARTNER_NAME = Loc::getMessage('PROJECT_PRICESORT_PARTNER_NAME');
        $this->PARTNER_URI = Loc::getMessage('PROJECT_PRICESORT_PARTNER_URI');
    }

    public function DoInstall()
    {
        $this->Install();
    }

    public function DoUninstall()
    {
        $this->Uninstall();
    }

    public function InstallAgent()
    {
        Project\Tools\Utility\Agent::add(
                '\Project\PriceSort\Agent\Price::process();', // имя функции
                $this->MODULE_ID, // идентификатор модуля
                "N", // агент не критичен к кол-ву запусков
                30 * 60, // интервал запуска - 1 сутки
                "", // дата первой проверки - текущее
                "Y", // агент активен
                "", // дата первого запуска - текущее
                30);
    }

    public function UnInstallAgent()
    {
        CAgent::RemoveAgent('\Project\PriceSort\Agent\Price::process();', $this->MODULE_ID);
    }

}
