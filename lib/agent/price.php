<?

namespace Project\PriceSort\Agent;

use Bitrix\Main\Application;
use CIBlockElement;
use CCatalogProduct;
use Project\PriceSort\Config;
use Exception;

class Price
{

    static public function process($page = 1)
    {
        try {
            Application::getInstance()->getConnection()->startTransaction();
            $page = self::update($page);
            Application::getInstance()->getConnection()->commitTransaction();
        } catch (Exception $e) {
            Application::getInstance()->getConnection()->rollbackTransaction();
        }
        return '\Project\PriceSort\Agent\Price::process(' . $page . ');';
    }

    static private function update($page)
    {
        $arSelect = ['ID', 'IBLOCK_ID', 'PROPERTY_MINIMUM_PRICE'];
        $arFilter = [
            'IBLOCK_ID' => Config::IBLOCK_ID,
            '=ACTIVE'   => 'Y',
        ];
        $res = CIBlockElement::GetList([], $arFilter, false, ['iNumPage' => $page, 'nPageSize' => Config::LIMIT],
            $arSelect);
        while ($arItem = $res->Fetch()) {
            $arDiscounts = CCatalogProduct::GetOptimalPrice($arItem['ID'], 1, array(2));
            if($arItem['PROPERTY_MINIMUM_PRICE_VALUE']!=$arDiscounts['DISCOUNT_PRICE']) {
                CIBlockElement::SetPropertyValues($arItem['ID'], $arItem['IBLOCK_ID'], $arDiscounts['DISCOUNT_PRICE'], 'MINIMUM_PRICE');
            }
        }
        if ($res->NavPageCount <= $page) {
            return 1;
        }
        return $page + 1;
    }

}
